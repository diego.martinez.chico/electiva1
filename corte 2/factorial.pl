factorial(1,1):- !.
factorial(N, F):-
    N2 is N-1,
    factorial(N2, F2),
    F is F2*N.
